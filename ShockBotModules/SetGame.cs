﻿using ShockBotAddin;
using System;
using Discord;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace ShockBotModules {
    public class SetGame : IShockBotAddin {
        public async Task HandleMessage(IDiscordClient client, IMessage eventArgs, string command, IList<string> parameters) {
            var socketClient = client as DiscordSocketClient;
            if (socketClient == null) { return; }
            string game = string.Join(" ", parameters);
            await socketClient.SetGameAsync(game);
        }
    }
}
