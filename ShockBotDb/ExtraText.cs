using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("ExtraText")]
    public partial class ExtraText {
        [PrimaryKey, Identity] public int Id      { get; set; } // int(11)
        [Column, NotNull     ] public string Text { get; set; } // text
        [Column, NotNull     ] public int Used    { get; set; } // int(1)

        #region Associations

        /// <summary>
        /// FK_ExtraTextTag_ExtraText_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ExtraTextId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ExtraTextTag> ExtraTextTags { get; set; }

        #endregion
    }
}
