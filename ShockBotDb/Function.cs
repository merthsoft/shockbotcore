using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("Function")]
    public partial class Function {
        [PrimaryKey, Identity] public int Id              { get; set; } // int(11)
        [Column, NotNull     ] public string TypeName     { get; set; } // text
        [Column, NotNull     ] public string AssemblyPath { get; set; } // text

        #region Associations

        /// <summary>
        /// FK_Response_Function_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "FunctionId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Response> Responses { get; set; }

        #endregion
    }
}
