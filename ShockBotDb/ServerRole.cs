using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerRole")]
    public partial class ServerRole {
        [PrimaryKey, NotNull] public ulong Id     { get; set; } // bigint(20) unsigned
        [Column, NotNull    ] public string Name  { get; set; } // text
        [Column, NotNull    ] public int Position { get; set; } // int(11)
        [Column, NotNull    ] public uint Color   { get; set; } // int(10) unsigned
        [Column, NotNull    ] public int IsActive { get; set; } // int(1)

        #region Associations

        /// <summary>
        /// FK_ServerRoleRestriction_ServerRole_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ServerRoleId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerRoleRestriction> ServerRoleRestrictions { get; set; }

        /// <summary>
        /// FK_ServerUserServerRole_ServerRole_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ServerRoleId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerUserServerRole> ServerUserServerRoles { get; set; }

        #endregion
    }
}
