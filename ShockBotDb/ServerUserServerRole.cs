using LinqToDB.Mapping;
using System;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerUserServerRole")]
    public partial class ServerUserServerRole {
        [Column, NotNull] public ulong ServerUserId { get; set; } // bigint(20) unsigned
        [Column, NotNull] public ulong ServerRoleId { get; set; } // bigint(20) unsigned

        #region Associations

        /// <summary>
        /// FK_ServerUserServerRole_ServerRole
        /// </summary>
        [Association(ThisKey = "ServerRoleId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerUserServerRole_ServerRole", BackReferenceName = "ServerUserServerRoles")]
        public ServerRole ServerRole { get; set; }

        /// <summary>
        /// FK_ServerUserServerRole_ServerUser
        /// </summary>
        [Association(ThisKey = "ServerUserId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerUserServerRole_ServerUser", BackReferenceName = "ServerUserServerRoles")]
        public ServerUser ServerUser { get; set; }

        #endregion
    }
}
