﻿using LinqToDB.Configuration;
using System;
using System.Collections.Generic;

namespace ShockBotDb {
    public class ConnectionSetting : ILinqToDBSettings {
        public class ConnectionStringSettings : IConnectionStringSettings {
            public string ConnectionString { get; set; }
            public string Name             { get; set; }
            public string ProviderName     { get; set; }
            public bool IsGlobal => false;
        }

        public IEnumerable<IDataProviderSettings> DataProviders { get { yield break; } }

        public string DefaultConfiguration => "MySql";

        public string DefaultDataProvider => "MySql";

        private string connectionString { get; set; }

        public ConnectionSetting(string server, string database, string user, string password, string protocol = "socket") {
            connectionString = $"Server={server};Database={database};Uid={user};Pwd={password};Protocol={protocol}";
        }

        public IEnumerable<IConnectionStringSettings> ConnectionStrings {
            get {
                yield return
                    new ConnectionStringSettings {
                        Name = "MySql",
                        ProviderName = "MySql",
                        ConnectionString = connectionString
                    };
            }
        }
    }
}
