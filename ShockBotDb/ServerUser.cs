using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerUser")]
    public partial class ServerUser {
        [PrimaryKey, NotNull] public ulong Id               { get; set; } // bigint(20) unsigned
        [Column, NotNull    ] public string CurrentUsername { get; set; } // text
        [Column, Nullable   ] public string CurrentNickname { get; set; } // text
        [Column, Nullable   ] public DateTime? LastSpoke    { get; set; } // datetime
        [Column, NotNull    ] public DateTime JoinedAt      { get; set; } // datetime
        [Column, Nullable   ] public DateTime? LastOnlineAt { get; set; } // datetime
        [Column, NotNull    ] public int IsBot              { get; set; } // int(1)
        [Column, NotNull    ] public int IsBanned           { get; set; } // int(1)
        [Column, NotNull    ] public int IsOnServer         { get; set; } // int(1)

        #region Associations

        /// <summary>
        /// FK_ServerUserNameHistory_ServerUser_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "UserId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerUserNameHistory> ServerUserNameHistories { get; set; }

        /// <summary>
        /// FK_ServerUserRestriction_ServerUser_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ServerUserId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerUserRestriction> ServerUserRestrictions { get; set; }

        /// <summary>
        /// FK_ServerUserServerRole_ServerUser_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ServerUserId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerUserServerRole> ServerUserServerRoles { get; set; }

        #endregion
    }
}
