﻿
CREATE TABLE `Command` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` text CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `CommandText` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CommandId` int(11) NOT NULL,
  `Text` text CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_CommandText_Command` (`CommandId`),
  CONSTRAINT `FK_CommandText_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ExtraText` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Text` text CHARACTER SET utf8mb4 NOT NULL,
  `Used` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1091 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ExtraTextTag` (
  `ExtraTextId` int(11) NOT NULL,
  `TagId` int(11) NOT NULL,
  PRIMARY KEY (`ExtraTextId`,`TagId`),
  KEY `FK_EXTRATEXTTAG_TAG` (`TagId`),
  CONSTRAINT `FK_ExtraTextTag_ExtraText` FOREIGN KEY (`ExtraTextId`) REFERENCES `ExtraText` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ExtraTextTag_Tag` FOREIGN KEY (`TagId`) REFERENCES `Tag` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `Function` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TypeName` text CHARACTER SET utf8mb4 NOT NULL,
  `AssemblyPath` text CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `File` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Path` text CHARACTER SET utf8mb4 NOT NULL,
  `Used` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `FileTag` (
  `FileId` int(11) NOT NULL,
  `TagId` int(11) NOT NULL,
  PRIMARY KEY (`FileId`,`TagId`),
  KEY `FK_IMAGETAG_IMAGE` (`FileId`),
  KEY `FK_IMAGETAG_TAG` (`TagId`),
  CONSTRAINT `FK_ImageTag_Image` FOREIGN KEY (`FileId`) REFERENCES `file` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ImageTag_Tag` FOREIGN KEY (`TagId`) REFERENCES `Tag` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `NameType` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `Response` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CommandId` int(11) NOT NULL,
  `NumParameters` int(11) NOT NULL DEFAULT '0',
  `Text` text CHARACTER SET utf8mb4,
  `FileTagId` int(11) DEFAULT NULL,
  `FunctionId` int(11) DEFAULT NULL,
  `ExtraTextTagId` int(11) DEFAULT NULL,
  `FailedChannelCheckText` text CHARACTER SET utf8mb4,
  `FailedRoleCheckText` text CHARACTER SET utf8mb4,
  `FailedUserCheckText` text CHARACTER SET utf8mb4,
  `FailedParameterCheckText` text CHARACTER SET utf8mb4,
  PRIMARY KEY (`Id`),
  KEY `FK_RESPONSE_COMMAND` (`CommandId`),
  KEY `FK_RESPONSE_EXTRATEXTTAG` (`ExtraTextTagId`),
  KEY `FK_RESPONSE_FUNCTION` (`FunctionId`),
  KEY `FK_RESPONSE_IMAGETAG` (`FileTagId`),
  CONSTRAINT `FK_Response_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Response_ExtraTextTag` FOREIGN KEY (`ExtraTextTagId`) REFERENCES `ExtraTextTag` (`TagId`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_Response_FileTag` FOREIGN KEY (`FileTagId`) REFERENCES `filetag` (`TagId`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_Response_Function` FOREIGN KEY (`FunctionId`) REFERENCES `Function` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerChannel` (
  `Id` bigint(20) unsigned NOT NULL,
  `Name` text CHARACTER SET utf8mb4 NOT NULL,
  `Private` int(1) NOT NULL DEFAULT '0',
  `IsActive` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerChannelRestriction` (
  `ServerChannelId` bigint(11) unsigned NOT NULL,
  `CommandId` int(11) NOT NULL,
  PRIMARY KEY (`ServerChannelId`,`CommandId`),
  KEY `FK_ServerChannelRestriction_Command` (`CommandId`),
  CONSTRAINT `FK_ServerChannelRestriction_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerChannelRestriction_ServerChannel` FOREIGN KEY (`ServerChannelId`) REFERENCES `ServerChannel` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerRole` (
  `Id` bigint(20) unsigned NOT NULL,
  `Name` text CHARACTER SET utf8mb4 NOT NULL,
  `Position` int(11) NOT NULL,
  `Color` int(10) unsigned NOT NULL,
  `IsActive` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerRoleRestriction` (
  `ServerRoleId` bigint(11) unsigned NOT NULL,
  `CommandId` int(11) NOT NULL,
  PRIMARY KEY (`ServerRoleId`,`CommandId`),
  KEY `FK_ServerRoleRestriction_Command` (`CommandId`),
  CONSTRAINT `FK_ServerRoleRestriction_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerRoleRestriction_ServerRole` FOREIGN KEY (`ServerRoleId`) REFERENCES `ServerRole` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerUser` (
  `Id` bigint(20) unsigned NOT NULL,
  `CurrentUsername` text CHARACTER SET utf8mb4 NOT NULL,
  `CurrentNickname` text CHARACTER SET utf8mb4,
  `LastSpoke` datetime DEFAULT NULL,
  `JoinedAt` datetime NOT NULL,
  `LastOnlineAt` datetime DEFAULT NULL,
  `IsBot` int(1) NOT NULL DEFAULT '0',
  `IsBanned` int(1) NOT NULL DEFAULT '0',
  `IsOnServer` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerUserNameHistory` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` bigint(20) unsigned NOT NULL,
  `Timestamp` datetime NOT NULL,
  `Before` text COLLATE utf8mb4_unicode_ci,
  `After` text COLLATE utf8mb4_unicode_ci,
  `NameTypeId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_UserNameHistory_ServerUser` (`UserId`),
  KEY `FK_ServerUserNameHistory_NameType` (`NameTypeId`),
  CONSTRAINT `FK_ServerUserNameHistory_NameType` FOREIGN KEY (`NameTypeId`) REFERENCES `NameType` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerUserNameHistory_ServerUser` FOREIGN KEY (`UserId`) REFERENCES `ServerUser` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerUserRestriction` (
  `ServerUserId` bigint(11) unsigned NOT NULL,
  `CommandId` int(11) NOT NULL,
  PRIMARY KEY (`ServerUserId`,`CommandId`),
  KEY `FK_ServerUserRestriction_Command` (`CommandId`),
  CONSTRAINT `FK_ServerUserRestriction_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerUserRestriction_ServerUser` FOREIGN KEY (`ServerUserId`) REFERENCES `ServerUser` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `ServerUserServerRole` (
  `ServerUserId` bigint(20) unsigned NOT NULL,
  `ServerRoleId` bigint(20) unsigned NOT NULL,
  KEY `FK_ServerUserServerRole_ServerRole` (`ServerRoleId`),
  KEY `FK_ServerUserServerRole_ServerUser` (`ServerUserId`),
  CONSTRAINT `FK_ServerUserServerRole_ServerRole` FOREIGN KEY (`ServerRoleId`) REFERENCES `ServerRole` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerUserServerRole_ServerUser` FOREIGN KEY (`ServerUserId`) REFERENCES `ServerUser` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Tag` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Tag` text CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
