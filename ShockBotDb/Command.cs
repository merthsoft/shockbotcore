using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("Command")]
    public partial class Command {
        [PrimaryKey, Identity] public int Id             { get; set; } // int(11)
        [Column, NotNull     ] public string Description { get; set; } // text

        #region Associations

        /// <summary>
        /// FK_CommandText_Command_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "CommandId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<CommandText> CommandTexts { get; set; }

        /// <summary>
        /// FK_Response_Command_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "CommandId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Response> Responses { get; set; }

        /// <summary>
        /// FK_ServerChannelRestriction_Command_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "CommandId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerChannelRestriction> ServerChannelRestrictions { get; set; }

        /// <summary>
        /// FK_ServerRoleRestriction_Command_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "CommandId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerRoleRestriction> ServerRoleRestrictions { get; set; }

        /// <summary>
        /// FK_ServerUserRestriction_Command_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "CommandId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerUserRestriction> ServerUserRestrictions { get; set; }

        #endregion
    }
}
