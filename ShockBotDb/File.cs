using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("File")]
    public partial class File {
        [Column, PrimaryKey, Identity] public int Id              { get; set; }
        [Column, NotNull             ] public string Path         { get; set; }
        [Column, NotNull             ] public int Used            { get; set; }

        #region Associations

        /// <summary>
        /// FK_FileTag_File_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "FileId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<FileTag> FileTags { get; set; }

        #endregion
    }
}
