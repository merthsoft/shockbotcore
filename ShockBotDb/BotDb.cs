using System;
using System.Collections.Generic;
using System.Linq;

using LinqToDB;
using LinqToDB.Mapping;

namespace ShockBotDb {
    /// <summary>
    /// Provides the wrapper around the bot's database
    /// </summary>
    public partial class BotDb : LinqToDB.Data.DataConnection {
        public ITable<Command> Commands                                   => GetTable<Command>();
        public ITable<CommandText> CommandTexts                           => GetTable<CommandText>();
        public ITable<ExtraText> ExtraTexts                               => this.GetTable<ExtraText>();
        public ITable<ExtraTextTag> ExtraTextTags                         => this.GetTable<ExtraTextTag>();
        public ITable<Function> Functions                                 => GetTable<Function>();
        public ITable<File> File                                          => GetTable<File>();
        public ITable<FileTag> FileTags                                   => GetTable<FileTag>();
        public ITable<Response> Responses                                 => GetTable<Response>();
        public ITable<ServerChannel> ServerChannels                       => GetTable<ServerChannel>();
        public ITable<ServerChannelRestriction> ServerChannelRestrictions => GetTable<ServerChannelRestriction>();
        public ITable<ServerRole> ServerRoles                             => GetTable<ServerRole>();
        public ITable<ServerRoleRestriction> ServerRoleRestrictions       => GetTable<ServerRoleRestriction>();
        public ITable<ServerUser> ServerUsers                             => GetTable<ServerUser>();
        public ITable<ServerUserNameHistory> ServerUserNameHistories      => GetTable<ServerUserNameHistory>();
        public ITable<ServerUserRestriction> ServerUserRestrictions       => GetTable<ServerUserRestriction>();
        public ITable<ServerUserServerRole> ServerUserServerRoles         => GetTable<ServerUserServerRole>();
        public ITable<Tag> Tags                                           => GetTable<Tag>();
        public ITable<NameType> NameTypes                                 => GetTable<NameType>();

        public BotDb() {
            InitDataContext();
        }

        public BotDb(string configuration)
            : base(configuration) {
            InitDataContext();
        }

        partial void InitDataContext();
    }
}
