using LinqToDB;
using System;

using System.Linq;

namespace ShockBotDb {
    public static partial class TableExtensions {
        public static Command Find(this ITable<Command> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static CommandText Find(this ITable<CommandText> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static ExtraText Find(this ITable<ExtraText> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static ExtraTextTag Find(this ITable<ExtraTextTag> table, int ExtraTextId, int TagId) {
            return table.FirstOrDefault(t =>
                t.ExtraTextId == ExtraTextId &&
                t.TagId == TagId);
        }

        public static Function Find(this ITable<Function> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static File Find(this ITable<File> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static FileTag Find(this ITable<FileTag> table, int ImageId, int TagId) {
            return table.FirstOrDefault(t =>
                t.FileId == ImageId &&
                t.TagId == TagId);
        }

        public static Response Find(this ITable<Response> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static ServerChannel Find(this ITable<ServerChannel> table, ulong Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static ServerChannelRestriction Find(this ITable<ServerChannelRestriction> table, ulong ServerChannelId, int CommandId) {
            return table.FirstOrDefault(t =>
                t.ServerChannelId == ServerChannelId &&
                t.CommandId == CommandId);
        }

        public static ServerRole Find(this ITable<ServerRole> table, ulong Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static ServerRoleRestriction Find(this ITable<ServerRoleRestriction> table, ulong ServerRoleId, int CommandId) {
            return table.FirstOrDefault(t =>
                t.ServerRoleId == ServerRoleId &&
                t.CommandId == CommandId);
        }

        public static ServerUser Find(this ITable<ServerUser> table, ulong Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static ServerUserNameHistory Find(this ITable<ServerUserNameHistory> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }

        public static Tag Find(this ITable<Tag> table, int Id) {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }
    }
}
