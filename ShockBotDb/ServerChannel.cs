using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerChannel")]
    public partial class ServerChannel {
        [PrimaryKey, NotNull] public ulong Id     { get; set; } // bigint(20) unsigned
        [Column, NotNull    ] public string Name  { get; set; } // text
        [Column, NotNull    ] public int Private  { get; set; } // int(1)
        [Column, NotNull    ] public int IsActive { get; set; } // int(1)

        #region Associations

        /// <summary>
        /// FK_ServerChannelRestriction_ServerChannel_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ServerChannelId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerChannelRestriction> ServerChannelRestrictions { get; set; }

        #endregion
    }
}
