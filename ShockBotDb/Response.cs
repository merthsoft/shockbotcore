using LinqToDB.Mapping;
using System;
using System.Linq;

namespace ShockBotDb {
    [Table("Response")]
    public partial class Response {
        [PrimaryKey, Identity] public int Id                          { get; set; }
        [Column, NotNull     ] public int CommandId                   { get; set; }
        [Column, NotNull     ] public int NumParameters               { get; set; }
        [Column, Nullable    ] public string Text                     { get; set; }
        [Column, Nullable    ] public int? FileTagId                  { get; set; }
        [Column, Nullable    ] public int? FunctionId                 { get; set; }
        [Column, Nullable    ] public int? ExtraTextTagId             { get; set; }
        [Column, Nullable    ] public string FailedChannelCheckText   { get; set; }
        [Column, Nullable    ] public string FailedRoleCheckText      { get; set; }
        [Column, Nullable    ] public string FailedUserCheckText      { get; set; }
        [Column, Nullable    ] public string FailedParameterCheckText { get; set; }

        #region Associations

        /// <summary>
        /// FK_Response_Command
        /// </summary>
        [Association(ThisKey = "CommandId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_Response_Command", BackReferenceName = "Responses")]
        public Command Command { get; set; }

        /// <summary>
        /// FK_Response_ExtraTextTag
        /// </summary>
        [Association(ThisKey = "ExtraTextTagId", OtherKey = "TagId", CanBeNull = true, KeyName = "FK_Response_ExtraTextTag", BackReferenceName = "Responses")]
        public ExtraTextTag ExtraTextTag { get; set; }

        /// <summary>
        /// FK_Response_Function
        /// </summary>
        [Association(ThisKey = "FunctionId", OtherKey = "Id", CanBeNull = true, KeyName = "FK_Response_Function", BackReferenceName = "Responses")]
        public Function Function { get; set; }

        /// <summary>
        /// FK_Response_FileTag
        /// </summary>
        [Association(ThisKey = "FileTagId", OtherKey = "TagId", CanBeNull = true, KeyName = "FK_Response_FileTag", BackReferenceName = "Responses")]
        public FileTag FileTag { get; set; }

        #endregion
    }
}
