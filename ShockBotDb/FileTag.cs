using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("FileTag")]
    public partial class FileTag {
        [PrimaryKey(-1), NotNull] public int FileId  { get; set; }
        [PrimaryKey(-1), NotNull] public int TagId   { get; set; }

        #region Associations

        /// <summary>
        /// FK_FileTag_Image
        /// </summary>
        [Association(ThisKey = "FileId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_FileTag_Image", BackReferenceName = "FileTags")]
        public File File { get; set; }

        /// <summary>
        /// FK_FileTag_Tag
        /// </summary>
        [Association(ThisKey = "TagId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_FileTag_Tag", BackReferenceName = "FileTags")]
        public Tag Tag { get; set; }

        /// <summary>
        /// FK_Response_FileTag_BackReference
        /// </summary>
        [Association(ThisKey = "TagId", OtherKey = "FileTagId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Response> Responses { get; set; }

        #endregion
    }
}
