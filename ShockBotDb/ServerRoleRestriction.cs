using LinqToDB.Mapping;
using System;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerRoleRestriction")]
    public partial class ServerRoleRestriction {
        [PrimaryKey(-1), NotNull] public ulong ServerRoleId { get; set; } // bigint(11) unsigned
        [PrimaryKey(-1), NotNull] public int CommandId      { get; set; } // int(11)

        #region Associations

        /// <summary>
        /// FK_ServerRoleRestriction_Command
        /// </summary>
        [Association(ThisKey = "CommandId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerRoleRestriction_Command", BackReferenceName = "ServerRoleRestrictions")]
        public Command Command { get; set; }

        /// <summary>
        /// FK_ServerRoleRestriction_ServerRole
        /// </summary>
        [Association(ThisKey = "ServerRoleId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerRoleRestriction_ServerRole", BackReferenceName = "ServerRoleRestrictions")]
        public ServerRole ServerRole { get; set; }

        #endregion
    }
}
