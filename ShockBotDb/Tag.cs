using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("Tag")]
    public partial class Tag {
        [Column(), PrimaryKey, Identity] public int Id            { get; set; } // int(11)
        [Column("Tag"), NotNull        ] public string Tag_Column { get; set; } // text

        #region Associations

        /// <summary>
        /// FK_ExtraTextTag_Tag_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "TagId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ExtraTextTag> ExtraTextTags { get; set; }

        /// <summary>
        /// FK_ImageTag_Tag_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "TagId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<FileTag> ImageTags { get; set; }

        #endregion
    }
}
