using LinqToDB.Mapping;
using System;
using System.Linq;

namespace ShockBotDb {
    [Table("CommandText")]
    public partial class CommandText {
        [PrimaryKey, Identity] public int Id        { get; set; } // int(11)
        [Column, NotNull     ] public int CommandId { get; set; } // int(11)
        [Column, NotNull     ] public string Text   { get; set; } // text

        #region Associations

        /// <summary>
        /// FK_CommandText_Command
        /// </summary>
        [Association(ThisKey = "CommandId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_CommandText_Command", BackReferenceName = "CommandTexts")]
        public Command Command { get; set; }

        #endregion
    }
}
