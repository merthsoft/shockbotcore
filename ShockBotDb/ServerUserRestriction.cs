using LinqToDB.Mapping;
using System;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerUserRestriction")]
    public partial class ServerUserRestriction {
        [Column, NotNull] public ulong ServerUserId { get; set; } // bigint(11) unsigned
        [Column, NotNull] public int CommandId      { get; set; } // int(11)

        #region Associations

        /// <summary>
        /// FK_ServerUserRestriction_Command
        /// </summary>
        [Association(ThisKey = "CommandId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerUserRestriction_Command", BackReferenceName = "ServerUserRestrictions")]
        public Command Command { get; set; }

        /// <summary>
        /// FK_ServerUserRestriction_ServerUser
        /// </summary>
        [Association(ThisKey = "ServerUserId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerUserRestriction_ServerUser", BackReferenceName = "ServerUserRestrictions")]
        public ServerUser ServerUser { get; set; }

        #endregion
    }
}
