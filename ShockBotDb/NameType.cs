using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("NameType")]
    public partial class NameType {
        public const string Username = "Username";
        public const string Nickname = "Nickname";

        [PrimaryKey, Identity] public int Id      { get; set; } // int(11)
        [Column, Nullable    ] public string Type { get; set; } // text

        #region Associations

        /// <summary>
        /// FK_ServerUserNameHistory_NameType_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "NameTypeId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ServerUserNameHistory> ServerUserNameHistories { get; set; }

        #endregion
    }
}
