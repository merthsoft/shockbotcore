using LinqToDB.Mapping;
using System;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerChannelRestriction")]
    public partial class ServerChannelRestriction {
        [PrimaryKey(-1), NotNull] public ulong ServerChannelId { get; set; } // bigint(11) unsigned
        [PrimaryKey(-1), NotNull] public int CommandId         { get; set; } // int(11)

        #region Associations

        /// <summary>
        /// FK_ServerChannelRestriction_Command
        /// </summary>
        [Association(ThisKey = "CommandId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerChannelRestriction_Command", BackReferenceName = "ServerChannelRestrictions")]
        public Command Command { get; set; }

        /// <summary>
        /// FK_ServerChannelRestriction_ServerChannel
        /// </summary>
        [Association(ThisKey = "ServerChannelId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerChannelRestriction_ServerChannel", BackReferenceName = "ServerChannelRestrictions")]
        public ServerChannel ServerChannel { get; set; }

        #endregion
    }
}
