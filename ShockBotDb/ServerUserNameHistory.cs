using LinqToDB.Mapping;
using System;
using System.Linq;

namespace ShockBotDb {
    [Table("ServerUserNameHistory")]
    public partial class ServerUserNameHistory {
        [PrimaryKey, Identity] public int Id             { get; set; } // int(11)
        [Column, NotNull     ] public ulong UserId       { get; set; } // bigint(20) unsigned
        [Column, NotNull     ] public DateTime Timestamp { get; set; } // datetime
        [Column, Nullable    ] public string Before      { get; set; } // text
        [Column, Nullable    ] public string After       { get; set; } // text
        [Column, NotNull     ] public int NameTypeId     { get; set; }

        #region Associations

        /// <summary>
        /// FK_ServerUserNameHistory_ServerUser
        /// </summary>
        [Association(ThisKey = "UserId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerUserNameHistory_ServerUser", BackReferenceName = "ServerUserNameHistories")]
        public ServerUser ServerUserNameHistoryServerUser { get; set; }

        /// <summary>
        /// FK_ServerUserNameHistory_NameType
        /// </summary>
        [Association(ThisKey = "NameTypeId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ServerUserNameHistory_NameType", BackReferenceName = "ServerUserNameHistories")]
        public NameType ServerUserNameHistoryNameType { get; set; }

        #endregion
    }
}
