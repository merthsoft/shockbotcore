using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShockBotDb {
    [Table("ExtraTextTag")]
    public partial class ExtraTextTag {
        [PrimaryKey(-1), NotNull] public int ExtraTextId { get; set; } // int(11)
        [PrimaryKey(-1), NotNull] public int TagId       { get; set; } // int(11)

        #region Associations

        /// <summary>
        /// FK_ExtraTextTag_ExtraText
        /// </summary>
        [Association(ThisKey = "ExtraTextId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ExtraTextTag_ExtraText", BackReferenceName = "ExtraTextTags")]
        public ExtraText ExtraText { get; set; }

        /// <summary>
        /// FK_ExtraTextTag_Tag
        /// </summary>
        [Association(ThisKey = "TagId", OtherKey = "Id", CanBeNull = false, KeyName = "FK_ExtraTextTag_Tag", BackReferenceName = "ExtraTextTags")]
        public Tag Tag { get; set; }

        /// <summary>
        /// FK_Response_ExtraTextTag_BackReference
        /// </summary>
        [Association(ThisKey = "TagId", OtherKey = "ExtraTextTagId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Response> Responses { get; set; }

        #endregion
    }
}
