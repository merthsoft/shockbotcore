﻿using Discord;
using Discord.WebSocket;
using LinqToDB;
using LinqToDB.Data;
using Microsoft.Extensions.Configuration;
using ShockBotAddin;
using ShockBotDb;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Threading.Tasks;

namespace ShockBotCore {
    partial class Bot {
        DiscordSocketClient client;
        readonly Random random = new Random();
        IConfigurationRoot config { get; set; }
        string fileRoot;

        public static void Main(string[] args) => new Bot().MainAsync(args).GetAwaiter().GetResult();
        
        public async Task MainAsync(string[] args) {
            AssemblyLoadContext.Default.Unloading += shutdown;

            config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

            DataConnection.DefaultSettings = new ConnectionSetting(config["database:server"], config["database:database"], 
                config["database:user"], config["database:password"], 
                config["database:socket"]
            );

            fileRoot = config["fileRoot"];

            client = new DiscordSocketClient();

            client.Log += OnLogMessage;
            client.Connected += Client_Connected;

            client.MessageReceived += Client_MessageReceived;
            client.GuildAvailable += Client_ServerAvailable;
            client.UserUpdated += Client_UserUpdated;
            client.GuildMemberUpdated += Client_UserUpdated;
            client.UserJoined += Client_UserJoined;
            client.UserBanned += Client_UserBanned;
            client.UserLeft += Client_UserLeft;
            client.UserUnbanned += Client_UserUnbanned;

            await client.LoginAsync(TokenType.Bot, config["bot:token"]);
            await client.StartAsync();

            await Task.Delay(-1);
        }

        private async void shutdown(AssemblyLoadContext obj) {
            await client.StopAsync();
        }

        private async Task Client_MessageReceived(SocketMessage arg) {
            using (var db = new BotDb()) {
                var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == arg.Author.Id);
                dbUser.LastSpoke = DateTime.UtcNow;
                db.Update(dbUser);
            }

            if (arg.Author.IsBot) { return; }

            string trimmedMessage = arg.Content.TrimEnd(' ');
            var msg = trimmedMessage.Split(' ');
            var command = msg[0].ToUpper();
            var args = msg.Skip(1).ToArray();

            try {
                bool success = await readDbCommandAsync(arg, command, args);
                if (!success) {
                    await readDbCommandAsync(arg, trimmedMessage, args);
                }
            } catch (Exception ex) {
                await OnLogMessage(new LogMessage(LogSeverity.Error, "Client_MessageReceived", $"Error processing {command}: {ex.Message}", ex));
            }
        }

        private void logInfo(string source, string format, params object[] args) {
            OnLogMessage(new LogMessage(LogSeverity.Info, source, string.Format(format, args), null));
        }

        private async Task<bool> readDbCommandAsync(SocketMessage eventArgs, string command, string[] args) { 
            using (var db = new BotDb()) {
                logInfo("readDbCommand", "Created db. Is null: {0}", db == null);

                try {
                    // Get exact-number-of-parameters command
                    var response = await (from r in db.Responses.LoadWith(r => r.Function)
                                    join ct in db.CommandTexts
                                    on r.CommandId equals ct.CommandId
                                    where ct.Text.ToUpper() == command.ToUpper()
                                    where r.NumParameters == args.Length
                                    select r).FirstOrDefaultAsync();
                    logInfo("readDbCommand", "First response object: {0}", response != null);
                    // Get command with any-number-of-parameters if that fails
                    if (response == null) {
                        response = await (from ct in db.CommandTexts
                                    join r in db.Responses.LoadWith(r => r.Function)
                                         on ct.CommandId equals r.CommandId
                                    where ct.Text.ToUpper() == command.ToUpper()
                                    where r.NumParameters < 0
                                    select r).FirstOrDefaultAsync();
                        logInfo("readDbCommand", "Second response object: {0}", response != null);
                    }

                    // And, finally, check if there's an "all text" command for this channel
                    if (response == null) {
                        response = await (from cr in db.ServerChannelRestrictions
                                    join r in db.Responses.LoadWith(r => r.Function)
                                         on cr.CommandId equals r.CommandId
                                    where cr.CommandId == -1
                                    where cr.ServerChannelId == eventArgs.Channel.Id
                                    select r).FirstOrDefaultAsync();
                        logInfo("readDbCommand", "Third response object: {0}", response != null);
                        if (response == null) {
                            return false;
                        }
                    }
                    await OnLogMessage(new LogMessage(LogSeverity.Verbose, "readDbCommand", $"SQL text: {db?.LastQuery}", null));

                    response.Command = db.Commands.FirstOrDefault(c => c.Id == response.CommandId);
                    logInfo("readDbCommand", "Command loaded: {0}", response.Command != null);

                    if (response.NumParameters == -2 && args.Length == 0) {
                        logInfo("readDbCommand", "Parameter mismatch.");
                        await SendMessage(eventArgs.Channel, response.FailedParameterCheckText, eventArgs.Author.Mention, "", "");
                        return false;
                    }

                    logInfo("readDbCommand", "Checking restrictions.");
                    if (!await checkResponseRestrictions(db, eventArgs, response)) {
                        logInfo("readDbCommand", "Restriction mismatch.");
                        return false;
                    }

                    logInfo("readDbCommand", "Outputting.");
                    await outputText(eventArgs, args, db, response);
                    await sendFile(eventArgs, db, response);
                    await handleFunction(eventArgs, db, command, args, response);

                    return true;
                } catch (Exception ex) {
                    await OnLogMessage(new LogMessage(LogSeverity.Verbose, "readDbCommand", $"Error occured. SQL text: {db?.LastQuery}", ex));
                    throw;
                }
            }
        }

        private static async Task SendMessage(ISocketMessageChannel channel, string format, params object[] args) {
            string message = string.Format(format, args);
            await channel.SendMessageAsync(message);
        }

        private async Task<bool> checkResponseRestrictions(BotDb db, SocketMessage eventArgs, Response response) {
            logInfo("checkResponseRestrictions", "Checking channel restrictions.");
            var restrictedChannels = db.ServerChannelRestrictions.Where(c => c.CommandId == response.CommandId).Select(c => c.ServerChannelId);
            if (restrictedChannels.Count() > 0) {
                var currentChannel = eventArgs.Channel;
                if (!restrictedChannels.Contains(currentChannel is ISocketPrivateChannel ? 0 : currentChannel.Id)) {
                    await SendMessage(eventArgs.Channel, response.FailedChannelCheckText, eventArgs.Author.Mention, "", "");
                    return false;
                }
            }

            logInfo("checkResponseRestrictions", "Checking roles restrictions.");
            var restrictedRoles = db.ServerRoleRestrictions.Where(r => r.CommandId == response.CommandId).Select(r => r.ServerRoleId);
            var guildUser = eventArgs.Author as SocketGuildUser;
            if (restrictedRoles.Count() > 0 && guildUser != null) {
                var userRoles = guildUser.Roles.Select(r => r.Id);
                
                if (userRoles.Count(r => restrictedRoles.Contains(r)) == 0) {
                    await SendMessage(eventArgs.Channel, response.FailedRoleCheckText, eventArgs.Author.Mention, "", "");
                    return false;
                }
            }

            logInfo("checkResponseRestrictions", "Checking user restrictions.");
            var restrictedUsers = db.ServerUserRestrictions.Where(u => u.CommandId == response.CommandId).Select(u => u.ServerUserId);
            if (restrictedUsers.Count() != 0) {
                if (!restrictedUsers.Contains(eventArgs.Author.Id)) {
                    await SendMessage(eventArgs.Channel, response.FailedUserCheckText, eventArgs.Author.Mention, "", "");
                    return false;
                }
            }

            return true;
        }

        private async Task handleFunction(SocketMessage eventArgs, BotDb db, string command, string[] args, Response response) {
            if (response.FunctionId != null) {
                logInfo("handleFunction", "Loading function with id: {0}", response.FunctionId);

                response.Function = db.Functions.FirstOrDefault(f => f.Id == response.FunctionId);

                if (response.Function == null) { return; }

                logInfo("handleFunction", "Checking for function: {0}::{1}", response.Function.AssemblyPath, response.Function.TypeName);
                var assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(response.Function.AssemblyPath);

                if (assembly == null) {
                    await OnLogMessage(new LogMessage(LogSeverity.Error, $"readDbCommand {command}", $"Assembly not found: {response.Function.AssemblyPath}", null));
                    await SendMessage(eventArgs.Channel, "{0}: Command {1} failed.", eventArgs.Author, command);
                }

                var type = assembly.GetType(response.Function.TypeName);

                if (type == null) {
                    await OnLogMessage(new LogMessage(LogSeverity.Error, $"readDbCommand {command}", $"Type {response.Function.TypeName} not found in assembly {response.Function.AssemblyPath}", null));
                    await SendMessage(eventArgs.Channel, "{0}: Command {1} failed.", eventArgs.Author, command);
                }

                var instance = Activator.CreateInstance(type) as IShockBotAddin;

                if (instance == null) {
                    await OnLogMessage(new LogMessage(LogSeverity.Error, $"readDbCommand {command}", $"Unable to create or cast {response.Function.AssemblyPath}::{response.Function.TypeName}", null));
                    await SendMessage(eventArgs.Channel, "{0}: Command {1} failed.", eventArgs.Author, command);
                }

                await OnLogMessage(new LogMessage(LogSeverity.Info, $"readDbCommand {command}", $"Calling function {response.Function.TypeName}"));
                await instance.HandleMessage(client, eventArgs, command, args);
                await OnLogMessage(new LogMessage(LogSeverity.Info, $"readDbCommand {command}", $"Function returned"));
            }
        }

        private async Task outputText(SocketMessage eventArgs, string[] args, BotDb db, Response response) {
            if (response.Text != null) {
                ExtraText extraText = null;
                // Get a random "extra text" based on tag
                if (response.ExtraTextTagId.HasValue) {
                    var query = db.ExtraTextTags.LoadWith(ett => ett.ExtraText).Where(ett => ett.TagId == response.ExtraTextTagId);
                    if (query.Count(ett => ett.ExtraText.Used == 0) == 0) {
                        db.ExtraTexts.Where(et => query.Select(ett => ett.ExtraTextId).Contains(et.Id)).Set(et => et.Used, 0).Update();
                    } else {
                        query = query.Where(ett => ett.ExtraText.Used == 0);
                    }
                    extraText = query.ElementAt(random.Next(query.Count())).ExtraText;
                    db.ExtraTexts.Where(et => et.Id == extraText.Id).Set(et => et.Used, 1).Update();
                }

                await SendMessage(eventArgs.Channel, response.Text, eventArgs.Author.Mention, string.Join(" ", args), extraText?.Text);
            }
        }

        private async Task sendFile(SocketMessage eventArgs, BotDb db, Response response) {
            if (response.FileTagId.HasValue) {
                // Get a random image based on tag
                var query = db.FileTags.LoadWith(it => it.File).Where(it => it.TagId == response.FileTagId);
                if (query.Count(it => it.File.Used == 0) == 0) {
                    db.File.Where(it => query.Select(i => i.FileId).Contains(it.Id)).Set(i => i.Used, 0).Update();
                } else {
                    query = query.Where(it => it.File.Used == 0);
                }
                var file = query.ElementAt(random.Next(query.Count())).File;

                db.File.Where(i => i.Id == file.Id).Set(i => i.Used, 1).Update();

                var path = Path.Combine(fileRoot, file.Path);
                await eventArgs.Channel.SendFileAsync(path);
            }
        }

        private async Task Client_Connected() {
            await client.SetGameAsync(config["bot:game"]);
        }

        private Task OnLogMessage(LogMessage msg) {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}