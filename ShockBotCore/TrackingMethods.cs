﻿using Discord.WebSocket;
using LinqToDB;
using ShockBotDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShockBotCore {
    partial class Bot {
        private async Task Client_UserUnbanned(SocketUser user, SocketGuild guild) {
            await Task.Run(() => {
                SocketGuildUser socketGuildUser = user as SocketGuildUser;
                if (socketGuildUser == null) { return; }

                using (var db = new BotDb()) {
                    var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == socketGuildUser.Id);
                    dbUser.IsBanned = 0;
                    db.BeginTransaction();
                    updateUser(db, socketGuildUser);
                    updateUserRole(db, socketGuildUser);
                    db.CommitTransaction();
                }
            });
        }

        private async Task Client_UserLeft(SocketGuildUser user) {
            await Task.Run(() => {
                using (var db = new BotDb()) {
                    var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == user.Id);
                    dbUser.IsOnServer = 0;
                    db.BeginTransaction();
                    updateUser(db, user);
                    updateUserRole(db, user);
                    db.CommitTransaction();
                }
            });
        }

        private async Task Client_UserBanned(SocketUser user, SocketGuild guild) {
            await Task.Run(() => {
                SocketGuildUser socketGuildUser = user as SocketGuildUser;
                if (socketGuildUser == null) { return; }

                using (var db = new BotDb()) {
                    var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == socketGuildUser.Id);
                    dbUser.IsBanned = 1;
                    dbUser.IsOnServer = 0;
                    db.BeginTransaction();
                    updateUser(db, socketGuildUser);
                    updateUserRole(db, socketGuildUser);
                    db.CommitTransaction();
                }
            });
        }

        private async Task Client_UserJoined(SocketGuildUser user) {
            await Task.Run(() => {
                using (var db = new BotDb()) {
                    db.BeginTransaction();
                    updateUser(db, user);
                    updateUserRole(db, user);
                    db.CommitTransaction();
                }
            });
        }

        private async Task Client_UserUpdated(SocketUser before, SocketUser after) {
            await Task.Run(() => {
                SocketGuildUser socketGuildUser = after as SocketGuildUser;
                if (socketGuildUser == null) { return; }

                using (var db = new BotDb()) {
                    db.BeginTransaction();
                    updateUser(db, socketGuildUser);
                    updateUserRole(db, socketGuildUser);
                    db.CommitTransaction();
                }
            });
        }

        private static void updateUser(BotDb db, SocketGuildUser u) {
            var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == u.Id);
            var updatedUser = new ServerUser() {
                Id = u.Id, CurrentUsername = u.Username, CurrentNickname = u.Nickname, IsBot = u.IsBot ? 1 : 0, IsBanned = 0,
                IsOnServer = 1, JoinedAt = u.JoinedAt.Value.DateTime, LastOnlineAt = DateTime.UtcNow, LastSpoke = dbUser?.LastSpoke
            };

            if (dbUser == null) {
                db.Insert(updatedUser);
            } else {
                db.Update(updatedUser);

                if (dbUser.CurrentUsername != updatedUser.CurrentUsername) {
                    var history = new ServerUserNameHistory() {
                        UserId = updatedUser.Id,
                        Before = dbUser.CurrentUsername,
                        After = updatedUser.CurrentUsername,
                        NameTypeId = (from nt in db.NameTypes where nt.Type == NameType.Username select nt.Id).First(),
                        Timestamp = DateTime.UtcNow
                    };
                    db.InsertWithIdentity(history);
                }

                if (dbUser.CurrentNickname != updatedUser.CurrentNickname) {
                    var history = new ServerUserNameHistory() {
                        UserId = updatedUser.Id,
                        Before = dbUser.CurrentNickname,
                        After = updatedUser.CurrentNickname,
                        NameTypeId = (from nt in db.NameTypes where nt.Type == NameType.Nickname select nt.Id).First(),
                        Timestamp = DateTime.UtcNow
                    };
                    db.InsertWithIdentity(history);
                }
            }
        }

        private static void updateUserRole(BotDb db, SocketGuildUser u) {
            db.ServerUserServerRoles.Delete(r => r.ServerUserId == u.Id);
            foreach (var role in u.Roles) {
                db.Insert(new ServerUserServerRole() { ServerUserId = u.Id, ServerRoleId = role.Id });
            }
        }

        private async Task Client_ServerAvailable(SocketGuild guild) {
            await Task.Run(() => {
                logInfo("Client_JoinedServer", "Processing roles.");
                using (var db = new BotDb()) {
                    db.BeginTransaction();
                    db.ServerRoles.Set(r => r.IsActive, 0).Update();

                    foreach (var r in guild.Roles) {
                        var updatedRole = new ServerRole() { Id = r.Id, Color = r.Color.RawValue, IsActive = 1, Name = r.Name, Position = r.Position };
                        var dbRole = db.ServerRoles.FirstOrDefault(sr => sr.Id == r.Id);

                        if (dbRole == null) {
                            db.Insert(updatedRole);
                        } else {
                            db.Update(updatedRole);
                        }
                    }

                    db.CommitTransaction();
                }

                logInfo("Client_JoinedServer", "Processing channels.");
                using (var db = new BotDb()) {
                    db.BeginTransaction();
                    db.ServerChannels.Set(r => r.IsActive, 0).Update();

                    foreach (var c in guild.Channels) {
                        var updatedChannel = new ServerChannel() { Id = c.Id, IsActive = 1, Name = c.Name, Private = 0 };
                        var dbChannel = db.ServerChannels.FirstOrDefault(sc => sc.Id == c.Id);

                        if (dbChannel == null) {
                            db.Insert(updatedChannel);
                        } else {
                            db.Update(updatedChannel);
                        }
                    }

                    db.CommitTransaction();
                }

                logInfo("Client_JoinedServer", "Processing users.");
                using (var db = new BotDb()) {
                    db.BeginTransaction();
                    db.ServerUsers.Set(u => u.IsOnServer, 0);

                    foreach (var u in guild.Users) {
                        updateUser(db, u);
                    }

                    db.CommitTransaction();
                }

                logInfo("Client_JoinedServer", "Processing user roles.");
                using (var db = new BotDb()) {
                    db.BeginTransaction();

                    foreach (var u in guild.Users) {
                        updateUserRole(db, u);
                    }

                    db.CommitTransaction();
                }
            });
        }
    }
}
