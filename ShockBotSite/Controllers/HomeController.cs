﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShockBotDb;
using LinqToDB;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ShockBotSite.Controllers {
    public class HomeController : Controller {
        private string fileRoot;

        public IActionResult Index() {
            using (var db = new BotDb()) {
                var commands = db.Commands.LoadWith(c => c.CommandTexts).OrderBy(c => c.Description).ToList();
                return View(commands);
            }
        }

        public IActionResult CommandDetails(int id) {
            using (var db = new BotDb()) {
                var command = db.Commands.LoadWith(c => c.CommandTexts).LoadWith(c => c.Responses).FirstOrDefault(c => c.Id == id);
                foreach (var response in command.Responses) {
                    response.FileTag = db.FileTags.LoadWith(i => i.Tag).FirstOrDefault(t => t.TagId == response.FileTagId);
                    response.Function = db.Functions.FirstOrDefault(f => f.Id == response.FunctionId);
                    response.ExtraTextTag = db.ExtraTextTags.LoadWith(e => e.Tag).FirstOrDefault(t => t.TagId == response.ExtraTextTagId);
                }
                return View(command);
            }
        }

        public IActionResult Images(int id) {
            using (var db = new BotDb()) {
                var paths = from i in db.File
                           join it in db.FileTags on i.Id equals it.FileId
                           where it.TagId == id
                           select i.Path;
                return View(paths);
            }
        }

        public IActionResult Texts(int tagId) {
            using (var db = new BotDb()) {
                var texts = from t in db.ExtraTexts
                             join ett in db.ExtraTextTags on t.Id equals ett.ExtraTextId
                             where ett.TagId == tagId
                             select t.Text;
                return View(texts);
            }
        }

        public IActionResult Error() {
            return View();
        }
    }
}
