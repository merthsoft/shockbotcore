﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShockBotAddin {
    public interface IShockBotAddin {
        Task HandleMessage(IDiscordClient client, IMessage eventArgs, string command, IList<string> parameters);
    }
}
